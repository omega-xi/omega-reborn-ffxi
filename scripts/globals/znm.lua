-----------------------------------
-- Zeni NM System + Helpers
-- Soultrapper         : !additem 18721
-- Blank Soul Plate    : !additem 18722
-- Soultrapper 2000    : !additem 18724
-- Blank HS Soul Plate : !additem 18725
-- Soul Plate          : !additem 2477
-- Sanraku & Ryo       : !pos -127.0 0.9 22.6 50
-----------------------------------
-- require("scripts/globals/items")
require("scripts/globals/keyitems")
require("scripts/globals/settings")
require("scripts/globals/status")
require("scripts/globals/common")
require("scripts/globals/magic")
require("scripts/globals/msg")
require("scripts/globals/npc_util")
require("scripts/globals/pankration")
require("scripts/globals/utils")
---------------------------------------------------------------------------------
local FAUNA_LIMIT = 10000 -- Zeni handed out per Fauna
local SUBJECT_OF_INTEREST_LIMIT = 10000 -- Zeni handed out per SubjectsOfInterest
---------------------------------------------------------------------------------

local lures =
{
    2580, 2581, 2582, 2577, 2578, 2579, 2574, 2575, 2576,
    2573, 2590, 2591, 2592, 2587, 2588, 2589, 2584, 2585,
    2586, 2583, 2600, 2601, 2602, 2597, 2598, 2599, 2594,
    2595, 2596, 2593, 2572
}

local seals =
{
    tpz.ki.MAROON_SEAL, tpz.ki.MAROON_SEAL, tpz.ki.MAROON_SEAL,
    tpz.ki.APPLE_GREEN_SEAL,tpz.ki.APPLE_GREEN_SEAL,tpz.ki.APPLE_GREEN_SEAL,
    tpz.ki.CHARCOAL_GREY_SEAL, tpz.ki.DEEP_PURPLE_SEAL, tpz.ki.CHESTNUT_COLORED_SEAL,
    tpz.ki.LILAC_COLORED_SEAL,
    tpz.ki.CERISE_SEAL,tpz.ki.CERISE_SEAL,tpz.ki.CERISE_SEAL,
    tpz.ki.SALMON_COLORED_SEAL,tpz.ki.SALMON_COLORED_SEAL,tpz.ki.SALMON_COLORED_SEAL,
    tpz.ki.PURPLISH_GREY_SEAL, tpz.ki.GOLD_COLORED_SEAL, tpz.ki.COPPER_COLORED_SEAL,
    tpz.ki.BRIGHT_BLUE_SEAL,
    tpz.ki.PINE_GREEN_SEAL,tpz.ki.PINE_GREEN_SEAL,tpz.ki.PINE_GREEN_SEAL,
    tpz.ki.AMBER_COLORED_SEAL,tpz.ki.AMBER_COLORED_SEAL,tpz.ki.AMBER_COLORED_SEAL,
    tpz.ki.FALLOW_COLORED_SEAL,tpz.ki.TAUPE_COLORED_SEAL,tpz.ki.SIENNA_COLORED_SEAL,
    tpz.ki.LAVENDER_COLORED_SEAL
}

tpz = tpz or {}
tpz.znm = tpz.znm or {}

tpz.items = tpz.items or {}
tpz.items["BLANK_SOUL_PLATE"] = 18722
tpz.items["BLANK_HIGH_SPEED_SOUL_PLATE"] = 18725
tpz.items["SOUL_PLATE"] = 2477

tpz.znm.seals = {
    tpz.ki.MAROON_SEAL, tpz.ki.MAROON_SEAL, tpz.ki.MAROON_SEAL,
    tpz.ki.APPLE_GREEN_SEAL,tpz.ki.APPLE_GREEN_SEAL,tpz.ki.APPLE_GREEN_SEAL,
    tpz.ki.CHARCOAL_GREY_SEAL, tpz.ki.DEEP_PURPLE_SEAL, tpz.ki.CHESTNUT_COLORED_SEAL,
    tpz.ki.LILAC_COLORED_SEAL,
    tpz.ki.CERISE_SEAL,tpz.ki.CERISE_SEAL,tpz.ki.CERISE_SEAL,
    tpz.ki.SALMON_COLORED_SEAL,tpz.ki.SALMON_COLORED_SEAL,tpz.ki.SALMON_COLORED_SEAL,
    tpz.ki.PURPLISH_GREY_SEAL, tpz.ki.GOLD_COLORED_SEAL, tpz.ki.COPPER_COLORED_SEAL,
    tpz.ki.BRIGHT_BLUE_SEAL,
    tpz.ki.PINE_GREEN_SEAL,tpz.ki.PINE_GREEN_SEAL,tpz.ki.PINE_GREEN_SEAL,
    tpz.ki.AMBER_COLORED_SEAL,tpz.ki.AMBER_COLORED_SEAL,tpz.ki.AMBER_COLORED_SEAL,
    tpz.ki.FALLOW_COLORED_SEAL,tpz.ki.TAUPE_COLORED_SEAL,tpz.ki.SIENNA_COLORED_SEAL,
    tpz.ki.LAVENDER_COLORED_SEAL
}

tpz.znm.lures = {
    2580, 2581, 2582, 2577, 2578, 2579, 2574, 2575, 2576,
    2573, 2590, 2591, 2592, 2587, 2588, 2589, 2584, 2585,
    2586, 2583, 2600, 2601, 2602, 2597, 2598, 2599, 2594,
    2595, 2596, 2593, 2572
}

tpz.znm.trophies = {
    2616, 2617, 2618, 2613, 2614, 2615, 2610, 2611, 2612,
    2609, 2626, 2627, 2628, 2623, 2624, 2625, 2620, 2621,
    2622, 2619, 2636, 2637, 2638, 2633, 2634, 2635, 2630,
    2631, 2632, 2629
}

-----------------------------------
-- Soultrapper
-----------------------------------
tpz.znm.soultrapper = tpz.znm.soultrapper or {}

tpz.znm.soultrapper.onItemCheck = function(target, user)
    if not user:isFacing(target) then
        return tpz.msg.basic.ITEM_UNABLE_TO_USE
    end

    local id = user:getEquipID(tpz.slot.AMMO)
    if
        id ~= tpz.items.BLANK_SOUL_PLATE and
        id ~= tpz.items.BLANK_HIGH_SPEED_SOUL_PLATE
    then
        return tpz.msg.basic.ITEM_UNABLE_TO_USE
    end

    if user:getFreeSlotsCount() == 0 then
        return tpz.msg.basic.FULL_INVENTORY
    end

    return 0
end

-- Good source for in era values https://www.bluegartr.com/threads/63949-Zeni-per-Image-rates
-- Key rate points
-- Aw'Euvhi Card NMs - 65-67ish - never match sub/fauna
-- Genbu - no match, Mid 80s
-- High Level large mobs - mid 40s no match sub/fauna
-- Bees/etc - 16ish with no match
tpz.znm.soultrapper.getZeniValue = function(target, user, showDebugMessage)
    local hpp = target:getHPP()
    local isFacing = target:isFacing(user)
    local targetLevel = target:getMainLvl()
    local isNM = target:isNM() -- ToDo consider if we should handle things like MOBTYPE_EVENT, MOBTYPE_BATTLEFIELD, and CHECK_AS_NM
    local distance = user:checkDistance(target)
    local modelSize = target:getModelSize()
    local isBeingIrresponsible = (user:getInstance() ~= nil) or (user:getBattlefield() ~= nil)

    if (showDebugMessage == nil) then
        showDebugMessage = false
    end

    local zeni = 0
    -- no claim  = little to no zeni
    if (not user:isMobOwner(target) or hpp == 100) then
        zeni = math.random(1,5)
    else
        -- base for small size mobs
        zeni = 16 
        -- large model mobs get a bonus, possibly up to 50 but based on working backwards from known subjects (Genbu, Aw'euvhi, etc it appears to be 2.5x)
        if (modelSize > 0) then
            zeni = zeni * 2.5
        end

        -- Level Component
        if (targetLevel > 75) then
            zeni = zeni + (targetLevel - 75)
        end

        -- NM/Rarity Component -- appears to be very substantial, outweighing even SubjectsOfInterest partial matches
        if isNM then
            local nmBonus = 0
            if (modelSize > 0) then
                nmBonus = 30
            else
                -- seems that small NMs get a bit bigger bonus, perhaps to offset the low base to begin with
                nmBonus = 40
            end
            zeni = zeni + nmBonus

            if isBeingIrresponsible then
                -- what are you doing taking pictures? you are in a battle?!
                -- proto-omega, salvalge bosses, etc all give more points than would be expected
                zeni = zeni + 20
            end
        end

        -- Distance Component
        if (distance > 5) then
            zeni = zeni * 0.5
        end

        -- Angle/Facing Component
        if not isFacing then
            zeni = zeni * 0.5
        end

         -- HP% Component    
        if (hpp > 50) then
            zeni = zeni * 0.25
        elseif (hpp > 25) then
            zeni = zeni * 0.50
        elseif (hpp > 5) then
            zeni = zeni * 0.75
        end

        -- per bgwiki - https://www.bg-wiki.com/ffxi/Category:Pankration#Purchasing_Items
        -- HS Soul Plate should allow for faster activation - not a bonus on points
        --[[if user:getEquipID(tpz.slot.AMMO) == tpz.items.BLANK_HIGH_SPEED_SOUL_PLATE then
            zeni = zeni * 1.5
        end]]

        -- safeguard, should never be possible but lets not deduct zeni, eh?
        if (zeni <= 0) then
            zeni = math.random(1, 5)
        end
        -- Sanitize Zeni
        zeni = math.floor(zeni) -- Remove any floating point information
    end
    
    if (showDebugMessage) then
        user:PrintToPlayer(string.format( "Zeni for Mob %s is %d!", target:getName(), zeni))
        user:PrintToPlayer(string.format( "MobSize [%d] HPP: [%d] isFacing: [%s] LevelOffset: [%d] Distance: [%d] isNM: [%s] hasClaim: [%s] isBeingIrresponsible: [%s]",
                                           modelSize, hpp, isFacing, (targetLevel - 75), distance, isNM, not (not user:isMobOwner(target) or hpp == 100), isBeingIrresponsible ))
    end

    return zeni
end

tpz.znm.soultrapper.onItemUse = function(target, item, user)
    -- TODO: There should be a failure chance - but finding any kind of rate seems difficult
    -- Determine Zeni starting value
    local zeni = tpz.znm.soultrapper.getZeniValue(target, user)

    -- Pick a skill totally at random...
    local skillIndex, skillEntry = utils.randomEntry(tpz.pankration.feralSkills)

    local faunaMatch, subjectsOfInterestMatch = getMatchingFaunaAndSubjectsOfInterest(target)

    -- Add plate
    local plate = user:addSoulPlate(target:getName(), faunaMatch, subjectsOfInterestMatch, target:getSystem(), zeni, skillIndex, skillEntry.fp)
    local data = plate:getSoulPlateData()
    -- utils.unused(data)
end

-----------------------------------
-- Ryo
-----------------------------------
tpz.znm.ryo = tpz.znm.ryo or {}

tpz.znm.ryo.onTrade = function(player, npc, trade)
    if npcUtil.tradeHasExactly(trade, tpz.items.SOUL_PLATE) then
        -- Cache the soulplate value on the player
        local item = trade:getItem(0)
        local plateData = item:getSoulPlateData()
        player:setLocalVar("[ZNM][Ryo]SoulPlateValue", plateData.zeni)
        player:startEvent(914)
    end
end

tpz.znm.ryo.onTrigger = function(player, npc)
    player:startEvent(913)
end

tpz.znm.ryo.onEventUpdate = function(player, csid, option)
    if csid == 914 then
        local zeniValue = player:getLocalVar("[ZNM][Ryo]SoulPlateValue")
        player:setLocalVar("[ZNM][Ryo]SoulPlateValue", 0)
        player:updateEvent(zeniValue)
    elseif csid == 913 then
        if option == 300 then
            player:updateEvent(player:getCurrency("zeni_point"), 0)
        elseif option == 200 then
            -- SubjectsOfInterest
            player:updateEvent(GetServerVariable("[ZNM]SubjectsOfInterest"))
        elseif option == 201 then
            -- Fauna
            player:updateEvent(GetServerVariable("[ZNM]Fauna"))
        elseif option == 400 then
            -- Islets dialog
             player:setLocalVar("[ZNM][Ryo]IsletDiscussion", 1)
        end
    end
end

tpz.znm.ryo.onEventFinish = function(player, csid, option)
end

-----------------------------------
-- Sanraku
-----------------------------------
tpz.znm.sanraku = tpz.znm.sanraku or {}

local platesTradedToday = function(player)
    local currentDay = VanadielDayAbsolute()
    local storedDay = player:getCharVar("[ZNM][Sanraku]TradingDay")

    if currentDay ~= storedDay then
        player:setCharVar("[ZNM][Sanraku]TradingDay", 0)
        player:setCharVar("[ZNM][Sanraku]TradedPlates", 0)
        return 0
    end

    return player:getCharVar("[ZNM][Sanraku]TradedPlates")
end

local function calculateZeniBonus(plateData)
    local zeni = plateData.zeni
    local faunaMatch = plateData.fauna
    local subOfInterestMatch = plateData.subOfInterest
    local ecosystem = plateData.ecoSystem

    local faunaKey = GetServerVariable("[ZNM]SubjectsOfInterest")
    local subjectsOfInterestKey = GetServerVariable("[ZNM]Fauna")

    if (faunaKey == 0) then
        faunaKey = 1 -- if there is no subject of interest var, take the first index for now
    end

    local isCurrentFauna = false
    local isCurrentSubjectsOfInterest = false
    local isCurrentEcoSytem = false

    if (GetServerVariable("[ZNM]SubjectsOfInterest") == subOfInterestMatch) then
        isCurrentSubjectsOfInterest = true
        zeni = zeni + 25
    end

    if (GetServerVariable("[ZNM]Ecosystem") == ecosystem) then
        isCurrentEcoSytem = true
        zeni = zeni + 25
    end

    if (GetServerVariable("[ZNM]Fauna") == faunaMatch) then
        isCurrentFauna = true
        zeni = zeni + 50
    end

    -- Sanitize Zeni
    zeni = math.floor(zeni) -- Remove any floating point information
    -- Add a little randomness
    zeni = zeni + math.random(-5, 5)
    -- clamp - highest reports in era are ~100ish
    zeni = utils.clamp(zeni, 1, 105)

    return zeni, isCurrentSubjectsOfInterest, isCurrentFauna, isCurrentEcoSytem
end

tpz.znm.sanraku.onTrade = function(player, npc, trade)
    if not player:hasKeyItem(tpz.ki.RHAPSODY_IN_AZURE) then
        if platesTradedToday(player) >= 10 then
            -- TODO: A message here? -- no message found in events
            return
        end
    else -- If you have the KI, clear out the tracking vars!
        player:setCharVar("[ZNM][Sanraku]TradingDay", 0)
        player:setCharVar("[ZNM][Sanraku]TradedPlates", 0)
    end

    if npcUtil.tradeHasExactly(trade, tpz.items.SOUL_PLATE) then
        -- Cache the soulplate value on the player
        local item = trade:getItem(0)
        local plateData = item:getSoulPlateData()
        player:setLocalVar("[ZNM][Sanraku]SoulPlateValue", plateData.zeni)
        player:startEvent(910, plateData.zeni)

    elseif trade:getItemCount() == 1 then
        local znm = -1
        found = false

        while znm <= 30 and not(found) do
            znm = znm + 1
            found = trade:hasItemQty(tpz.znm.trophies[znm + 1],1)
        end

        if (found) then
            local seal = tpz.znm.seals[znm+1]
            -- TODO make sure seal exists...?

            if player:hasKeyItem(seal) == false then
                player:tradeComplete()
                player:addKeyItem(seal)
                player:startEvent(912,0,0,0,seal)
            else
                player:messageSpecial(ID.text.SANCTION + 8,seal) -- You already possess .. (not sure this is authentic)
            end
        end
    end
end

------------------------------------------------------------
-- Gets the allowed ZNMs for a player based on KIs
-- pulled from Sanraku.lua
------------------------------------------------------------
local function getAllowedZNMs(player)
    local param = 2140136440 -- Defaut bitmask, Tier 1 ZNM Menu + don't ask option

    -- Tinnin Path
    if player:hasKeyItem(tpz.ki.MAROON_SEAL) then
        param = param - 0x38 -- unlocks Tinnin path tier 2 ZNMs.
    end
    if player:hasKeyItem(tpz.ki.APPLE_GREEN_SEAL) then
        param = param - 0x1C0 -- unlocks Tinnin path tier 3 ZNMs.
    end
    if player:hasKeyItem(tpz.ki.CHARCOAL_GREY_SEAL) and player:hasKeyItem(tpz.ki.DEEP_PURPLE_SEAL) and player:hasKeyItem(tpz.ki.CHESTNUT_COLORED_SEAL) then
        param = param - 0x200 -- unlocks Tinnin.
    end

    -- Sarameya Path
    if player:hasKeyItem(tpz.ki.CERISE_SEAL) then
        param = param - 0xE000 -- unlocks Sarameya path tier 2 ZNMs.
    end
    if player:hasKeyItem(tpz.ki.SALMON_COLORED_SEAL) then
        param = param - 0x70000 -- unlocks Sarameya path tier 3 ZNMs.
    end
    if player:hasKeyItem(tpz.ki.PURPLISH_GREY_SEAL) and player:hasKeyItem(tpz.ki.GOLD_COLORED_SEAL) and player:hasKeyItem(tpz.ki.COPPER_COLORED_SEAL) then
        param = param - 0x80000 -- unlocks Sarameya.
    end

    -- Tyger Path
    if player:hasKeyItem(tpz.ki.PINE_GREEN_SEAL) then
        param = param - 0x3800000 -- unlocks Tyger path tier 2 ZNMs.
    end
    if player:hasKeyItem(tpz.ki.AMBER_COLORED_SEAL) then
        param = param - 0x1C000000 -- unlocks Tyger path tier 3 ZNMs.
    end
    if player:hasKeyItem(tpz.ki.TAUPE_COLORED_SEAL) and player:hasKeyItem(tpz.ki.FALLOW_COLORED_SEAL) and player:hasKeyItem(tpz.ki.SIENNA_COLORED_SEAL) then
        param = param - 0x20000000 -- unlocks Tyger.
    end

    if player:hasKeyItem(tpz.ki.LILAC_COLORED_SEAL) and player:hasKeyItem(tpz.ki.BRIGHT_BLUE_SEAL) and player:hasKeyItem(tpz.ki.LAVENDER_COLORED_SEAL) then
        param = param - 0x40000000 -- unlocks Pandemonium Warden.
    end

    return param
end

tpz.znm.sanraku.onTrigger = function(player, npc)
    if player:getCharVar("[ZNM][Sanraku]Intro") == 0 then
        player:startEvent(908) -- 908: First time introduction
    else
        local param = 2140136440 -- Defaut bitmask, Tier 1 ZNM Menu + don't ask option

        -- Tinnin Path
        if player:hasKeyItem(tpz.ki.MAROON_SEAL) then
            param = param - 0x38 -- unlocks Tinnin path tier 2 ZNMs.
        end
        if player:hasKeyItem(tpz.ki.APPLE_GREEN_SEAL) then
            param = param - 0x1C0 -- unlocks Tinnin path tier 3 ZNMs.
        end
        if player:hasKeyItem(tpz.ki.CHARCOAL_GREY_SEAL) and player:hasKeyItem(tpz.ki.DEEP_PURPLE_SEAL) and player:hasKeyItem(tpz.ki.CHESTNUT_COLORED_SEAL) then
            param = param - 0x200 -- unlocks Tinnin.
        end

        -- Sarameya Path
        if player:hasKeyItem(tpz.ki.CERISE_SEAL) then
            param = param - 0xE000 -- unlocks Sarameya path tier 2 ZNMs.
        end
        if player:hasKeyItem(tpz.ki.SALMON_COLORED_SEAL) then
            param = param - 0x70000 -- unlocks Sarameya path tier 3 ZNMs.
        end
        if player:hasKeyItem(tpz.ki.PURPLISH_GREY_SEAL) and player:hasKeyItem(tpz.ki.GOLD_COLORED_SEAL) and player:hasKeyItem(tpz.ki.COPPER_COLORED_SEAL) then
            param = param - 0x80000 -- unlocks Sarameya.
        end

        -- Tyger Path
        if player:hasKeyItem(tpz.ki.PINE_GREEN_SEAL) then
            param = param - 0x3800000 -- unlocks Tyger path tier 2 ZNMs.
        end
        if player:hasKeyItem(tpz.ki.AMBER_COLORED_SEAL) then
            param = param - 0x1C000000 -- unlocks Tyger path tier 3 ZNMs.
        end
        if player:hasKeyItem(tpz.ki.TAUPE_COLORED_SEAL) and player:hasKeyItem(tpz.ki.FALLOW_COLORED_SEAL) and player:hasKeyItem(tpz.ki.SIENNA_COLORED_SEAL) then
            param = param - 0x20000000 -- unlocks Tyger.
        end

        if player:hasKeyItem(tpz.ki.LILAC_COLORED_SEAL) and player:hasKeyItem(tpz.ki.BRIGHT_BLUE_SEAL) and player:hasKeyItem(tpz.ki.LAVENDER_COLORED_SEAL) then
            param = param - 0x40000000 -- unlocks Pandemonium Warden.
        end

        player:startEvent(909,param) -- 909: Further interactions
    end
end

tpz.znm.sanraku.onEventUpdate = function(player, csid, option)
    --printf("updateRESULT: %u",option)
    if csid == 909 then
        local zeni = player:getCurrency("zeni_point")

        if option >= 300 and option <= 302 then
            if option == 300 then
                salt = tpz.ki.SICKLEMOON_SALT
            elseif option == 301 then
                salt = tpz.ki.SILVER_SEA_SALT
            elseif option == 302 then
                salt = tpz.ki.CYAN_DEEP_SALT
            end
            if zeni < 500 then
                player:updateEvent(2,500) -- not enough zeni
            elseif player:hasKeyItem(salt) then
                player:updateEvent(3,500) -- has salt already
            else
                player:updateEvent(1,500,0,salt)
                player:addKeyItem(salt)
                player:delCurrency("zeni_point", 500)
            end
        else -- player is interested in buying a pop item.
            n = option % 10

            if n <= 2 then
                if option == 130 or option == 440 then
                    tier = 5
                else
                    tier = 1
                end
            elseif n >= 3 and n <= 5 then
                tier = 2
            elseif n >= 6 and n <= 8 then
                tier = 3
            else
                tier = 4
            end

            cost = tier * 1000 -- static pricing for now.

            if option >= 100 and option <= 130 then
                player:updateEvent(0,0,0,0,0,0,cost)
            elseif option >= 400 and option <=440 then
                if option == 440 then
                    option = 430
                end

                item = tpz.znm.lures[option-399]

                if option == 430 then -- Pandemonium Warden
                    keyitem1 = tpz.ki.LILAC_COLORED_SEAL keyitem2 = tpz.ki.BRIGHT_BLUE_SEAL keyitem3 = tpz.ki.LAVENDER_COLORED_SEAL
                elseif option == 409 then -- Tinnin
                    keyitem1 = tpz.ki.CHARCOAL_GREY_SEAL keyitem2 = tpz.ki.DEEP_PURPLE_SEAL keyitem3 = tpz.ki.CHESTNUT_COLORED_SEAL
                elseif option == 419 then -- Sarameya
                    keyitem1 = tpz.ki.PURPLISH_GREY_SEAL keyitem2 = tpz.ki.GOLD_COLORED_SEAL keyitem3 = tpz.ki.COPPER_COLORED_SEAL
                elseif option == 429 then -- Tyger
                    keyitem1 = tpz.ki.TAUPE_COLORED_SEAL keyitem2 = tpz.ki.FALLOW_COLORED_SEAL keyitem3 = tpz.ki.SIENNA_COLORED_SEAL
                else
                    keyitem1 = tpz.znm.seals[option - 402] keyitem2 = nil keyitem3 = nil
                end
                if cost > zeni then
                    player:updateEvent(2, cost, item, keyitem1,keyitem2,keyitem3) -- you don't have enough zeni.
                elseif player:addItem(item) then
                    if keyitem1 ~= nil then
                        player:delKeyItem(keyitem1)
                    end
                    if keyitem2 ~= nil then
                        player:delKeyItem(keyitem2)
                    end
                    if keyitem3 ~= nil then
                        player:delKeyItem(keyitem3)
                    end

                    player:updateEvent(1, cost, item, keyitem1,keyitem2,keyitem3)
                    player:delCurrency("zeni_point", cost)
                else
                    player:updateEvent(4, cost, item, keyitem1,keyitem2,keyitem3) -- Cannot obtain.
                end
            elseif option == 500 then -- player has declined to buy a pop item
                player:updateEvent(1,1) -- restore the "Gaining access to the islets" option.
            else
                --print("onEventSelection - CSID:",csid)
                --print("onEventSelection - option ===",option)
            end
        end
    end
end

tpz.znm.sanraku.onEventFinish = function(player, csid, option)
    if csid == 910 then
        player:confirmTrade()
        player:setCharVar("[ZNM][Sanraku]TradingDay", VanadielDayAbsolute())
        player:addCharVar("[ZNM][Sanraku]TradedPlates", 1)

        local zeniValue = player:getLocalVar("[ZNM][Sanraku]SoulPlateValue")
        player:setLocalVar("[ZNM][Sanraku]SoulPlateValue", 0)

        player:addCurrency("zeni_point", zeniValue)
    elseif csid == 908 then
        player:setCharVar("[ZNM][Sanraku]Intro",1)
        player:addCurrency("zeni_point", 2000)
    end

end
