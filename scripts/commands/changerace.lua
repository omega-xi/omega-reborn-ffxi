---------------------------------------------------------------------------------------------------
-- func: changerace
-- desc: Changes a players race, face, and size.
---------------------------------------------------------------------------------------------------

cmdprops =
{
    permission = 1,
    parameters = "ssss"
};

function error(player, msg)
    player:PrintToPlayer(msg);
    player:PrintToPlayer("!changerace <player> <face> <race> {size}");
end;

function onTrigger(player, arg1, arg2, arg3, arg4)
    local target;
    local face;
    local race;
    local size;

    target = GetPlayerByName(arg1);

    if (target == nil) then
        error(player, "Invalid player name.");
        return;
    end

    face = tonumber(arg2)
    race = tonumber(arg3)
    size = tonumber(arg4) or 1

    -- validate face
    if (face == nil) then
        error(player, "You must enter a face.");
        return;
    else
        if (face > 16 or face < 1) then
            error(player, "Invalid face. Face must be between 1 and 16");
            return;
        end
    end

    -- validate race
    if (race == nil) then
        error(player, "You must enter a race.");
        return;
    else
        if (race > 8 or race < 1) then
            error(player, "Invalid race. Race must be between 1 and 8");
            return;
        end
    end

    -- validate size
    if (size > 3 or size < 1) then
        error(player, "Invalid size. Size must be between 1 and 3");
        return;
    end

    -- change race
    if (target:setRace(face, race, size-1)) then
        target:setPos(target:getXPos(), target:getYPos(), target:getZPos(), target:getRotPos(), target:getZoneID());
        player:PrintToPlayer(string.format("Race change for %s was successful.", target:getName()));
    else
        player:PrintToPlayer(string.format("Race change for %s failed.", target:getName()));
    end
end