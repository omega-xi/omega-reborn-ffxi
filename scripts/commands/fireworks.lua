---------------------------------------------------------------------------------------------------
-- func: Fireworks!
-- auth: Swerve
-- desc: FREAKING FIREWORKS!
---------------------------------------------------------------------------------------------------

cmdprops =
{
    permission = 0,
    parameters = ""
};

function error(player, msg)
    player:PrintToPlayer(msg);
    player:PrintToPlayer("!fireworks");
end;

function onTrigger(player)
		stock = {
				4186, 100, -- Airborne
				5441, 100, -- Angelwing
				4216, 100, -- Brilliant Snow
				4167, 100, -- Cracker
				4250, 100, -- Crackler
				5361, 100, -- Datechochin
				4251, 100, -- Festive Fan
				5725, 100, -- Goshikitenge
				4184, 100, -- Kongou Inaho
				4183, 100, -- Konron Hassen
				4169, 100, -- Little Comet
				4185, 100, -- Meifu Goma
				5360, 100, -- Muteppo
				4256, 100, -- Ouka Ranman
				4257, 100, -- Papillion
				5769, 100, -- Popper (Congrats!)
				4217, 100, -- Sparkling Hand
				4253, 100, -- Spirit Masque
				4252, 100, -- Summer Fan
				4168, 100, -- Twinkle Shower
				15179, 100,
				14520, 100,
				11968, 100,
				11966, 100,
				10383, 100,
				15753, 100,
				18864, 100,
				}

		tpz.shop.general(player, stock);
end
