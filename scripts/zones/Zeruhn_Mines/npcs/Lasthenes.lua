-----------------------------------
-- Area: Zeruhn Mines
--  NPC: Lasthenes
-- Notes: Opens Gate
-----------------------------------

function onTrade(player, npc, trade)
end

function onTrigger(player, npc)
    player:setCharVar("[lasthenes]opengate", 0)
	if player:getXPos() > -79.5 then
        player:startEvent(180)
    else
        player:startEvent(181)
    end
end

function onEventUpdate(player, csid, option)
    --[[Note: onEventUpdate only fires if the player chooses option "Let me through"
        I would prefer to check the option variable in the onEventFinish but it is always 0
        this is a workaround so that we don't force the player through the door if they choose "Not right now"]]
    player:setCharVar("[lasthenes]opengate", 1)
end

function onEventFinish(player, csid, option)
    local opengate = player:getCharVar("[lasthenes]opengate")

    if opengate ~= 0 then
        -- Another quirk is that the player:getXPos() will always appear to be past the gate (is this something forced by the cutscene?)
        -- So we will wait until the player regains control of their character before checking their position
        player:queue(1000, checkPos(player, csid))
    end
end

function checkPos(player, csid)
    return function(entity)
        -- Check if we need to force the player past the gate
        if csid == 181 and entity:getXPos() < -79.5 then
            entity:setPos(-77.4260, 0.0000, 19.8820, 0)
        end
    
        if csid == 180 and entity:getXPos() > -79.5 then
            entity:setPos(-82.5230, 0.2861, 19.9410, 128)
        end
    end
end