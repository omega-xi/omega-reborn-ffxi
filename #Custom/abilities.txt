#Fix Abilities that are tagged WOTG
Update abilities SET content_tag = "TOAU" Where name = "retaliation";
Update abilities SET content_tag = "TOAU" Where name = "elemental_siphon";
Update abilities SET content_tag = "TOAU" Where name = "accomplice";
Update abilities SET content_tag = "TOAU" Where name = "collaborator"; 
Update abilities SET content_tag = "TOAU" Where name = "sekkanoki"; 
Update abilities SET content_tag = "TOAU" Where name = "pianissimo"; 

#2 hours = 1 hour
Update abilities SET recastTime = 3600 Where name = "meikyo_shisui";
Update abilities SET recastTime = 3600 Where name = "eagle_eye_shot";
Update abilities SET recastTime = 3600 Where name = "soul_voice";
Update abilities SET recastTime = 3600 Where name = "familiar";
Update abilities SET recastTime = 3600 Where name = "blood_weapon";
Update abilities SET recastTime = 3600 Where name = "invincible";
Update abilities SET recastTime = 3600 Where name = "perfect_dodge";
Update abilities SET recastTime = 3600 Where name = "benediction";
Update abilities SET recastTime = 3600 Where name = "chainspell";
Update abilities SET recastTime = 3600 Where name = "manafont";
Update abilities SET recastTime = 3600 Where name = "hundred_fists";
Update abilities SET recastTime = 3600 Where name = "mighty_strikes";
Update abilities SET recastTime = 3600 Where name = "mijin_gakure";
Update abilities SET recastTime = 3600 Where name = "astral_flow";
